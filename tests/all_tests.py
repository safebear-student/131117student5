import base_test


class TestCases(base_test.BaseTest):

    def test_01_test_login(self):
        # Step 1: Check we're on the Welcome page
        assert self.welcomepage.check_page()
        # Step 2: Check on Login and the Login Page Loads
        assert self.welcomepage.click_login(self.loginpage)
        # Step 3: Login to the webpage and confirm the main page appears
        assert self.loginpage.login(self.mainpage, "testuser", "testing")
        # Step 4: Return to Welcome page after initial login
        assert self.mainpage.click_logout(self.welcomepage)

